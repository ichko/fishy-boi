import RPi.GPIO as GPIO
import time

class CarNavigator:
    def __init__(self, steer_coef=2):
        self.speed = 1.0
        self.steer_coef = steer_coef

        PWM_FREQUENCY = 500

        self.LEFT_SPEED_PIN = 11
        self.RIGHT_SPEED_PIN = 22

        self.LEFT_CTL_PIN_1 = 10
        self.LEFT_CTL_PIN_2 = 9
        self.RIGHT_CTL_PIN_1 = 17
        self.RIGHT_CTL_PIN_2 = 27

        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BCM)

        GPIO.setup(self.LEFT_SPEED_PIN, GPIO.OUT)
        GPIO.setup(self.RIGHT_SPEED_PIN, GPIO.OUT)
        
        GPIO.setup(self.LEFT_CTL_PIN_1, GPIO.OUT)
        GPIO.setup(self.LEFT_CTL_PIN_2, GPIO.OUT)
        GPIO.setup(self.RIGHT_CTL_PIN_1, GPIO.OUT)
        GPIO.setup(self.RIGHT_CTL_PIN_2, GPIO.OUT)

        self.PWM_LEFT_SPEED_PIN = GPIO.PWM(self.LEFT_SPEED_PIN, PWM_FREQUENCY)
        self.PWM_RIGHT_SPEED_PIN = GPIO.PWM(self.RIGHT_SPEED_PIN, PWM_FREQUENCY)

    def forward(self):
        self.set_tire_speeds(self.speed, self.speed)

    def backwards(self):
        self.set_tire_speeds(-self.speed, -self.speed)

    def left(self):
        self.set_tire_speeds(0, self.speed)

    def right(self):
        self.set_tire_speeds(self.speed, 0)

    def forward_left(self): 
        self.set_tire_speeds(self.speed / self.steer_coef, self.speed)

    def forward_right(self):
        self.set_tire_speeds(self.speed, self.speed / self.steer_coef)

    def backwards_left(self): 
        self.set_tire_speeds(-self.speed / self.steer_coef, -self.speed)

    def backwards_right(self):
        self.set_tire_speeds(-self.speed, -self.speed / self.steer_coef)

    def stop(self):
        self.set_tire_speeds(0, 0)

    def set_tire_speeds(self, left_tire_speed, right_tire_speed):
        left_control_pin_1_value = left_tire_speed < 0
        left_control_pin_2_value = left_tire_speed > 0

        right_control_pin_1_value = right_tire_speed > 0
        right_control_pin_2_value = right_tire_speed < 0

        self.PWM_LEFT_SPEED_PIN.start(abs(left_tire_speed * 100))
        GPIO.output(self.LEFT_CTL_PIN_1, left_control_pin_1_value)
        GPIO.output(self.LEFT_CTL_PIN_2, left_control_pin_2_value)

        self.PWM_RIGHT_SPEED_PIN.start(abs(right_tire_speed * 100))
        GPIO.output(self.RIGHT_CTL_PIN_1, right_control_pin_1_value)
        GPIO.output(self.RIGHT_CTL_PIN_2, right_control_pin_2_value)

        print(self.RIGHT_CTL_PIN_1)
        print(right_control_pin_1_value)
        


if __name__ == '__main__':
    car = CarNavigator()
    car.backwards()

    try:
        while True:
            pass
    except KeyboardInterrupt:
        car.stop()
        GPIO.cleanup()
    
