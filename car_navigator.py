import requests
import time


class CarNavigator:
    def __init__(self, address, speed, steer_coef, timeout=0.1):
        self.address = address
        self.speed = speed
        self.steer_coef = steer_coef
        self.timeout = timeout

    def forward(self):
        self.set_tire_speeds(self.speed, self.speed)

    def backwards(self):
        self.set_tire_speeds(-self.speed, -self.speed)

    def left(self):
        self.set_tire_speeds(0, self.speed)

    def right(self):
        self.set_tire_speeds(self.speed, 0)

    def forward_left(self): 
        self.set_tire_speeds(self.speed / self.steer_coef, self.speed)

    def forward_right(self):
        self.set_tire_speeds(self.speed, self.speed / self.steer_coef)

    def backwards_left(self): 
        self.set_tire_speeds(-self.speed / self.steer_coef, -self.speed)

    def backwards_right(self):
        self.set_tire_speeds(-self.speed, -self.speed / self.steer_coef)

    def stop(self):
        self.set_tire_speeds(0, 0)

    def set_tire_speeds(self, left, right):
        url = 'http://%s/c/%04d/%04d' % (self.address, int(left), int(right))
        try:
            print('requesting %s' % url)
            requests.get(url, timeout=self.timeout)
        except:
            print('request failed')


if __name__ == '__main__':
    import keyboard

    print('Input car server address: ')
    address = input()
    
    print('Input requests per second [10]: ')
    requests_per_second = input()

    if address == '':
        address = '192.168.43.3'

    if requests_per_second == '':
        requests_per_second = 10
    else:
        requests_per_second = int(requests_per_second)

    timeout = 1 / requests_per_second

    car = CarNavigator(address, 999, 2, timeout)

    while True:
        if keyboard.is_pressed('w'):
            if keyboard.is_pressed('a'):
                car.forward_left()
            elif keyboard.is_pressed('d'):
                car.forward_right()
            else:
                car.forward()
        elif keyboard.is_pressed('s'):
            if keyboard.is_pressed('a'):
                car.backwards_left()
            elif keyboard.is_pressed('d'):
                car.backwards_right()
            else:
                car.backwards()
        elif keyboard.is_pressed('a'):
                car.left()
        elif keyboard.is_pressed('d'):
                car.right()
        else:
                car.stop()
            
        time.sleep(timeout)
