#include <ESP8266WiFi.h>

WiFiServer server(80);

int LEFT_SPEED_PIN = D0;
int RIGHT_SPEED_PIN = D1;

int LEFT_CTL_PIN_1 = D8;
int LEFT_CTL_PIN_2 = D4;
int RIGHT_CTL_PIN_1 = D2;
int RIGHT_CTL_PIN_2 = D3;


void setup_tires();
void set_tires(int left_tire_speed, int right_tire_speed);

void connect_to_wifi(char* ssid, char* password);
void setup_server();
String get_request();


void setup() {
  Serial.begin(115200);
  delay(10);

  connect_to_wifi("TPLink", "tortatorta");
  setup_server();

  setup_tires();
  set_tires(0, 0);
}
 
void loop() {
  String request = get_request();

  if (request != "" && request.indexOf("/c") >= 0) {
    request.replace("+", "");
    
    int left_speed = request.substring(7, 11).toInt();
    int right_speed = request.substring(12, 16).toInt();

    Serial.println(left_speed);
    Serial.println(right_speed);
    
    set_tires(left_speed, right_speed);
  }
}


//////////////////
// NAVIGATION  ///
//////////////////

void setup_tires() {
  pinMode(LEFT_SPEED_PIN, OUTPUT);
  pinMode(RIGHT_SPEED_PIN, OUTPUT);
 
  pinMode(LEFT_CTL_PIN_1, OUTPUT);
  pinMode(LEFT_CTL_PIN_2, OUTPUT);
  pinMode(RIGHT_CTL_PIN_1, OUTPUT);
  pinMode(RIGHT_CTL_PIN_2, OUTPUT);
}

void set_tires(int left_tire_speed, int right_tire_speed) {
  int left_control_pin_1_value = left_tire_speed > 0 ? LOW : HIGH;
  int left_control_pin_2_value = left_tire_speed > 0 ? HIGH : LOW;

  int right_control_pin_1_value = right_tire_speed > 0 ? HIGH : LOW;
  int right_control_pin_2_value = right_tire_speed > 0 ? LOW : HIGH;

  analogWrite(LEFT_SPEED_PIN, abs(left_tire_speed));
  digitalWrite(LEFT_CTL_PIN_1, left_control_pin_1_value);
  digitalWrite(LEFT_CTL_PIN_2, left_control_pin_2_value);

  analogWrite(RIGHT_SPEED_PIN, abs(right_tire_speed));
  digitalWrite(RIGHT_CTL_PIN_1, right_control_pin_1_value);
  digitalWrite(RIGHT_CTL_PIN_2, right_control_pin_2_value);
}


//////////////////
// NETWORK     ///
//////////////////

String get_request() {
  // Check if a client has connected
  WiFiClient client = server.available();
  if (!client) {
    return "";
  }
 
  // Wait until the client sends some data
  Serial.println("new client");
  while(!client.available()){
    delay(1);
  }
 
  // Read the first line of the request
  String request = client.readStringUntil('\r');
  Serial.println(request);
  client.flush();

  // Return the response
  client.println("HTTP/1.1 200 OK");
  client.println("Content-Type: text/html");
  client.println(""); //  do not forget this one

  delay(1);
  Serial.println("Client disconnected");
  Serial.println("");

  return request;
}

void setup_server() {
  server.begin();
  
  Serial.println("Server started");
 
  // Print the IP address
  Serial.print("Use this URL : ");
  Serial.print("http://");
  Serial.print(WiFi.localIP());
  Serial.println("/");
}

void connect_to_wifi(char* ssid, char* password) {
  // Connect to WiFi network
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
 
  WiFi.begin(ssid, password);
 
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
}

