import keyboard
import requests
import time

left = 0
right = 0

speed = 999
steer_coef = 2


def steer(left, right):
	url = 'http://192.168.0.104/c/%04d/%04d' % (int(left), int(right))
	try:
		requests.get(url)
	except:
		pass


while True:
	left = right = 0

	if keyboard.is_pressed('s'):
		left = right = -speed
		
	if keyboard.is_pressed('w'):
		left = right = speed
	
	if keyboard.is_pressed('w') or keyboard.is_pressed('s'):
		if keyboard.is_pressed('a'):
			left = speed / steer_coef
		if keyboard.is_pressed('d'):
			right = speed / steer_coef
	
	if not keyboard.is_pressed('w') and not keyboard.is_pressed('s'):
		if keyboard.is_pressed('a'):
			left = 0
			right = speed
		elif keyboard.is_pressed('d'):
			left = speed
			right = 0
		
	steer(left, right)
	print(left, right)
	
	time.sleep(0.3)
