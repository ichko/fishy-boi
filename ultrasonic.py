import time
from multiprocessing import Process, Value

import RPi.GPIO as GPIO


def calc_and_set_distance(trigger, echo, distance):
    GPIO.setmode(GPIO.BCM)

    GPIO.setup(trigger, GPIO.OUT)
    GPIO.setup(echo, GPIO.IN)

    while True:
        time.sleep(0.1)

        # set trigger to HIGH
        GPIO.output(trigger, True)

        # set trigger after 0.01ms to LOW
        time.sleep(0.00001)
        GPIO.output(trigger, False)

        start_time = time.time()
        stop_time = time.time()

        # save stop_time
        while GPIO.input(echo) == 0:
            start_time = time.time()

        # save time of arrival
        while GPIO.input(echo) == 1:
            stop_time = time.time()

        # time difference between start and arrival
        time_elapsed = stop_time - start_time
        # multiply with the sonic speed (34300 cm/s)
        # and divide by 2, because there and back
        distance.value = (time_elapsed * 34300) / 2.0


class Ultrasonic:
    def __init__(self, trigger, echo):
        self.distance_value = Value('d', 0.0)

        self.process = Process(
            target=calc_and_set_distance,
            args=(trigger, echo, self.distance_value)
        )

        self.process.start()

    def distance(self):
        return self.distance_value.value

    def __del__(self):
        self.process.terminate()
        GPIO.cleanup()


if __name__ == '__main__':
    ultrasonic = Ultrasonic(25, 8)

    try:
        while True:
            dist = ultrasonic.distance()

            print("Measured Distance = %.1f cm" % dist)
            time.sleep(1)

        # Reset by pressing CTRL + C
    except KeyboardInterrupt:
        print("Measurement stopped by User")
